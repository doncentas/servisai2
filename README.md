# Coffin store

## Launch the app

Follow steps below in order to launch the application:

1. ```docker-compose up -d``` - to build and run the application.
## How to use the api

### GET ALL

```aidl
curl --location --request GET 'http://localhost:80/coffins' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
```

### GET BY ID

```
curl --location --request GET 'http://localhost:80/coffins/1' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
```

### DELETE by id

```aidl
curl --location --request DELETE 'http://localhost:80/coffins/1' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
```

### POST - create coffin

```aidl
curl --location --request POST 'http://localhost:80/coffins' \
--header 'Content-Type: application/json' \
--header 'accept: application/json' \
--data-raw '{
	"material": "wood",
	"length": 213,
	"width": 50,
	"height": 60,
    "deadManEmail": ""
}'
```

### PUT - update coffin

```aidl
curl --location --request PUT 'http://localhost:80/coffins/1' \
--header 'Content-Type: application/json' \
--header 'accept: application/json' \
--data-raw '{
	"material": "wood",
	"length": 213,
	"width": 55,
	"height": 60,
    "deadManEmail": ""
}'
```

### PATCH - patch coffin

```aidl
curl --location --request PATCH 'http://localhost:80/coffins/1' \
--header 'Content-Type: application/json' \
--header 'accept: application/json' \
--data-raw '{
	"width": 40
}'
```

### POST - create customer

```$xslt
curl --location --request POST 'http://localhost:80/customers' \
--header 'Content-Type: application/json' \
--data-raw '{
    "firstName": "Donatas",
    "lastName": "Cepaitis",
    "email": "donatas@gmail.com"
}
'
```

### GET - get customer by email

```$xslt
curl --location --request GET 'http://localhost:80/customers/donatas@gmail.com' \
--header 'Content-Type: application/json'
```

### GET - get all customers

```$xslt
curl --location --request GET 'http://localhost:80/customers' \
--header 'Content-Type: application/javascript' \
--data-raw ''
```