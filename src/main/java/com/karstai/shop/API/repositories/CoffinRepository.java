package com.karstai.shop.API.repositories;

import com.karstai.shop.API.models.Coffin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoffinRepository extends JpaRepository<Coffin, Long> {
    boolean existsById(Long id);
}
