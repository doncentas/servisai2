package com.karstai.shop.API.controllers;

import com.karstai.shop.API.models.DeadMan;
import com.karstai.shop.API.services.DeadManService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class DeadManController {

    private final DeadManService deadManService;

    public DeadManController(DeadManService deadManService) {
        this.deadManService = deadManService;
    }

    @GetMapping
    public List<DeadMan> getAllDeadMans() {
        return deadManService.getAllDeadMans();
    }

    @GetMapping("/{email}")
    public DeadMan getDeadManByEmail(@PathVariable final String email) {
        return deadManService.getDeadManByEmail(email);
    }

    @PostMapping
    public ResponseEntity<DeadMan> createDeadMan(@RequestBody final DeadMan deadMan) {
        return deadManService.createDeadMan(deadMan);
    }

}