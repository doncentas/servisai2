package com.karstai.shop.API.controllers;

import com.karstai.shop.API.dto.CoffinDto;
import com.karstai.shop.API.models.Coffin;
import com.karstai.shop.API.services.CoffinService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/coffins")
@RequiredArgsConstructor
public class CoffinController {

    private final CoffinService coffinService;

    @PostMapping
    public ResponseEntity<CoffinDto> create(@RequestBody Coffin coffin) {
        CoffinDto dbCoffin = coffinService.create(coffin);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("location",
                "/coffins/" + dbCoffin.getId());
        try {
            return ResponseEntity.created(new URI("/products/" + dbCoffin.getId())).headers(responseHeaders).body(dbCoffin);
        } catch (Exception e) {
            throw new RuntimeException("Failed creating response");
        }
    }

    @PutMapping("/{id}")
    public CoffinDto update(@PathVariable Long id, @RequestBody Coffin coffin) {
        return coffinService.update(id, coffin);
    }

    @PatchMapping("/{id}")
    public CoffinDto patchUpdate(@PathVariable Long id, @RequestBody Coffin coffin) {
        return coffinService.patchUpdate(id, coffin);
    }

    @GetMapping
    public List<CoffinDto> getAll() {
        return coffinService.getAll();
    }

    @GetMapping("/{id}")
    public CoffinDto getById(@PathVariable Long id) {
        return coffinService.getById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        coffinService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
