package com.karstai.shop.API.services;

import com.karstai.shop.API.dto.CoffinDto;
import com.karstai.shop.API.exceptions.EntityNotFoundException;
import com.karstai.shop.API.models.Coffin;
import com.karstai.shop.API.models.DeadMan;
import com.karstai.shop.API.repositories.CoffinRepository;
import com.karstai.shop.API.utils.NullAwareBeanUtilsBean;
import lombok.RequiredArgsConstructor;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CoffinService {
    private final CoffinRepository coffinRepository;
    private final DeadManService deadManService;

    public CoffinDto create(Coffin coffin) {
        if(coffin.getDeadManEmail() != null && !coffin.getDeadManEmail().isEmpty()) {
            DeadMan deadMan = deadManService.getDeadManByEmailOrCreate(coffin.getDeadManEmail());
            if (deadMan != null) {
                coffin.setDeadManEmail(deadMan.getEmail());
            }
        } else {
            coffin.setDeadManEmail(null);
        }
        return toCoffinDto(coffinRepository.save(coffin));
    }

    public CoffinDto getById(Long id) {
        Coffin coffin = coffinRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Coffin not found"));
        return toCoffinDto(coffin);
    }

    public CoffinDto update(Long id, Coffin coffin) {
        Coffin dbCoffin = coffinRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Coffin not found"));
        coffin.setId(dbCoffin.getId());

        assignUser(coffin, dbCoffin, true);

        try {
            BeanUtils.copyProperties(dbCoffin, coffin);
        } catch (Exception e) {
            throw new RuntimeException("ERROR");
        }

        return toCoffinDto(coffinRepository.save(dbCoffin));
    }

    public CoffinDto patchUpdate(Long id, Coffin coffin) {
        BeanUtilsBean beanUtilsBean = new NullAwareBeanUtilsBean();
        Coffin dbCoffin = coffinRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Coffin not found"));

        assignUser(coffin, dbCoffin, false);

        try {
            beanUtilsBean.copyProperties(dbCoffin, coffin);
        } catch (Exception e) {
            throw new RuntimeException("ERROR");
        }

        return toCoffinDto(coffinRepository.save(dbCoffin));
    }

    public List<CoffinDto> getAll() {
        return coffinRepository.findAll().stream()
                .map(this::toCoffinDto)
                .collect(Collectors.toList());
    }

    private void assignUser(Coffin coffin, Coffin dbCoffin, boolean nullable) {
        if(coffin.getDeadManEmail() != dbCoffin.getDeadManEmail()) {
            if(coffin.getDeadManEmail().isEmpty()) {
                if(nullable || coffin.getDeadManEmail().equals("")) {
                    dbCoffin.setDeadManEmail(null);
                }
            } else {
                DeadMan deadMan = deadManService.getDeadManByEmailOrCreate(coffin.getDeadManEmail());
                if (deadMan != null) {
                    dbCoffin.setDeadManEmail(deadMan.getEmail());
                }
            }
        }
    }

    private CoffinDto toCoffinDto(Coffin coffin) {
        CoffinDto coffinDto = new CoffinDto();
        coffinDto.setHeight(coffin.getHeight());
        coffinDto.setId(coffin.getId());
        coffinDto.setLength(coffin.getLength());
        coffinDto.setMaterial(coffin.getMaterial());
        if(coffin.getDeadManEmail() != null && !coffin.getDeadManEmail().isEmpty()) {
            coffinDto.setDeadMan(deadManService.getDeadManByEmail(coffin.getDeadManEmail()));
        }
        return coffinDto;
    }

    public void delete(Long id) {
        if(!coffinRepository.existsById(id)) {
            throw new EntityNotFoundException("Coffin not found");
        }
        coffinRepository.deleteById(id);
    }
}
