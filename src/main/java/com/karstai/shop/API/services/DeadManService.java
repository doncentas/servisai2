package com.karstai.shop.API.services;

import com.karstai.shop.API.models.DeadMan;
import com.karstai.shop.API.models.DeadManResponse;
import com.karstai.shop.API.models.DeadMansResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static com.karstai.shop.API.constants.Urls.BASE_USERS_URI;

@Service
@Log4j2
public class DeadManService {

    public List<DeadMan> getAllDeadMans() {
        return getDeadMans();
    }

    private List<DeadMan> getDeadMans() {
        RestTemplate restTemplate = new RestTemplate();
        try {
            DeadMansResponse usersResponse = restTemplate.getForObject(BASE_USERS_URI, DeadMansResponse.class);
            return usersResponse.getData();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public ResponseEntity<DeadMan> createDeadMan(final DeadMan deadMan) {
        DeadManResponse userResponse = saveDeadman(deadMan);
        DeadMan dbDeadMan = userResponse.getData();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("location", "/customers/" + dbDeadMan.getEmail());

        try {
            return ResponseEntity
                    .created(new URI("/customers/" + deadMan.getEmail()))
                    .headers(responseHeaders)
                    .body(dbDeadMan);
        } catch (Exception e) {
            throw new RuntimeException("Failed creating response");
        }
    }

    private DeadManResponse saveDeadman(DeadMan deadMan) {
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<DeadMan> request = new HttpEntity<>(deadMan);
        try {
            return restTemplate.postForObject(BASE_USERS_URI, request, DeadManResponse.class);
        } catch (Exception e) {
            log.error("Deadman creation failed: {}", deadMan.getEmail());
            return null;
        }
    }

    public DeadMan getDeadManByEmail(final String email) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate
                    .getForObject(BASE_USERS_URI + "/" + email, DeadManResponse.class)
                    .getData();
        } catch (Exception e) {
            log.error("Deadman Not Found: {}", email);
            return null;
        }
    }

    public DeadMan getDeadManByEmailOrCreate(final String email) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate
                    .getForObject(BASE_USERS_URI + "/" + email, DeadManResponse.class)
                    .getData();
        } catch (Exception e) {
            DeadMan deadMan = DeadMan.builder().email(email).build();
            saveDeadman(deadMan);
            return deadMan;
        }
    }
}