package com.karstai.shop.API.dto;

import com.karstai.shop.API.models.DeadMan;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CoffinDto {
    private Long id;

    private BigDecimal width;

    private BigDecimal height;

    private BigDecimal length;

    private String material;

    private DeadMan deadMan;
}
