package com.karstai.shop.API.models;

import lombok.Data;

@Data
public class DeadManResponse {
    private String message;
    private DeadMan data = null;
}
