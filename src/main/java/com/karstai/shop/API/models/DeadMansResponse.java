package com.karstai.shop.API.models;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DeadMansResponse implements Serializable {
    private String message;
    private List<DeadMan> data = null;
}

