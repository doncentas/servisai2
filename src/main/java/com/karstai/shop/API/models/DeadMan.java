package com.karstai.shop.API.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeadMan {
    private String firstName;
    private String lastName;
    private String email;
}
